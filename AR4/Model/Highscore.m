//
//  Highscore.m
//  AR4
//
//  Created by Avery Lamp on 4/12/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "Highscore.h"

@implementation Highscore

-(NSString *)toString
{
    NSString *tString = [NSString stringWithFormat:@"%@-%d", self.name, self.score];
    return tString;
}
-(instancetype) initWithString: (NSString *) initString
{
    self = [super init];
    if (self)
    {
        int index;
        //NSLog(initString);
        for (int i=0; i<initString.length; i++)
        {
            if ([initString characterAtIndex:i]  == '-'){
                index = i;
                break;
            }
        }
        self.name = [[NSString alloc]initWithString:[initString substringToIndex: index]];
        self.score = [[initString substringFromIndex:index + 1] intValue];
    }
    
    return self;
}

@end
