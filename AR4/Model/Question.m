//
//  Question.m
//  ActionResearchProject1st
//
//  Created by Avery Lamp on 3/10/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "Question.h"




@implementation Question

-(NSString *)toString
{
    
    
    NSString *toString = [[NSString alloc] init];
    
    NSString *question= [self.question stringByAppendingString:@"\n"];
    
    NSString *answer = [self.answer stringByAppendingString:@"\n"];
    
    NSString *sstring = [self.source stringByAppendingString:@"\n"];
    
    NSString *incorrects = @"";
    for (int i = 0; i <[self.incorrectAnswers count]; i++){
        incorrects = [incorrects stringByAppendingString:@"Wrong - "];
        
        incorrects = [incorrects stringByAppendingString:self.incorrectAnswers  [i]];

        incorrects = [incorrects stringByAppendingString:@"\n"];
        
        
        
    }
    
    toString = [question stringByAppendingString:answer];
    toString = [toString stringByAppendingString:incorrects];
    toString = [toString stringByAppendingString:sstring];
    return toString;
}

-(NSMutableArray *)incorrectAnswers
{
    if (!_incorrectAnswers )
    {
        _incorrectAnswers = [[NSMutableArray alloc] init];
    }
    return _incorrectAnswers;
}

-(NSMutableArray *)getRandomAnswers
{
    NSMutableArray *rAnswers = [[NSMutableArray alloc]initWithArray:[self.incorrectAnswers mutableCopy]];
    NSUInteger count = [rAnswers count];
    for (NSUInteger i = 0; i < count; ++i) {
        int nElements = count - i;
        int n = (random() % nElements) + i;
        [rAnswers exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    return rAnswers;
}

-(instancetype) initWithQuestion:(Question *)question
{
    self = [super init];
    
    if (self)
    {
        self.answer = question.answer;
        self.question = question.question;
        self.incorrectAnswers = [[NSMutableArray alloc]initWithArray:[question.incorrectAnswers mutableCopy]];
        
    }
    
    return self;
    
}


@end
