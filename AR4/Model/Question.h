//
//  Question.h
//  ActionResearchProject1st
//
//  Created by Avery Lamp on 3/10/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Question : NSObject

@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) NSString *answer;
@property (strong, nonatomic) NSMutableArray *incorrectAnswers;
@property(strong,nonatomic) NSString *source;
-(NSString *)toString;
-(NSMutableArray *)getRandomAnswers;
-(instancetype) initWithQuestion:(Question *)question;
@end
