//
//  Highscore.h
//  AR4
//
//  Created by Avery Lamp on 4/12/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Highscore : NSObject
@property (nonatomic)int score;
@property (strong, nonatomic) NSString *name;
-(NSString *) toString;
-(instancetype) initWithString:(NSString *)initString;


@end
