//
//  MCQuestionGame.h
//  ActionResearchProject2nd
//
//  Created by Avery Lamp on 3/11/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"Question.h"
@interface MCQuestionGame : NSObject

@property (strong, nonatomic) NSMutableArray *Questions;
@property(nonatomic) int numberToAsk;

-(Question *)getRandomQuestion;
-(void) resetGame;
-(instancetype)initWithNumberToAsk:(int) number;
@end
