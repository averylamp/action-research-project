//
//  MCQuestionGame.m
//  ActionResearchProject2nd
//
//  Created by Avery Lamp on 3/11/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "MCQuestionGame.h"
#import "Question.h"
@implementation MCQuestionGame

-(instancetype) initWithNumberToAsk: (int) number
{
    self=  [super init];
    if (self)
    {
        self.numberToAsk = number;
    }
    return self;
}

- (NSMutableArray *)Questions
{
    if (!_Questions){
        _Questions = [[NSMutableArray alloc]init];
        [self addQuestionsToGame];
    }
    return _Questions;
}
-(int) numberToAsk
{
    if(!_numberToAsk || _numberToAsk ==0)
    {
        _numberToAsk = 10;
    }
    return _numberToAsk;
}

- (void) addQuestionsToGame
{
    //NSLog(@"Reading File");
    NSString* filePath = @"Data";//file path...
    
    
    NSString* fileRoot = [[NSBundle mainBundle]
                          pathForResource:filePath ofType:@"txt"];
    
    NSString* fileContents =
    [NSString stringWithContentsOfFile:fileRoot
                              encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
    NSArray* allLinedStrings =
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    
    
   
    NSString* strsInOneLine =
    [allLinedStrings objectAtIndex:0];
    int numberOfQuestions = [strsInOneLine intValue];
    
    /*
    for (NSString *s in allLinedStrings){
        NSLog(s);
    }
    */
    
    int line = 1;
    int i=0;
    NSString *currentLine = allLinedStrings [line];
    
    while (i <numberOfQuestions){
        Question *currentQuestion = [[Question alloc]init];
        
        currentQuestion.question = [currentLine substringFromIndex:1];
        line ++;
        currentLine = allLinedStrings [line];
        currentQuestion.answer = currentLine;
        
        line ++;
        
        while (line < [allLinedStrings count]){
            currentLine = allLinedStrings [line];
            if ([currentLine characterAtIndex:0] == '?'){
                break;
            }else if([currentLine characterAtIndex:0] =='#'){
                currentQuestion.source = [currentLine substringFromIndex:1];
            }
            else{
                [currentQuestion.incorrectAnswers addObject: currentLine];
                
            }
            line ++;
        }
        
        [self.Questions addObject: currentQuestion];
        i++;
        //NSLog([currentQuestion toString]);
    }

    
}

-(void)resetGame
{
    _Questions = nil;
}


-(Question *)getRandomQuestion
{
    //NSLog(@"YES");
    Question *randomQuestion = nil;
    if ([self.Questions count]){
        unsigned index = arc4random() % [self.Questions count];
        randomQuestion = self.Questions [index];
        [self.Questions removeObjectAtIndex:index];
        
    }
    return randomQuestion;
}


@end
