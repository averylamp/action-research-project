//
//  EnterHighScoreViewController.h
//  AR4
//
//  Created by Avery Lamp on 4/20/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterHighScoreViewController : UIViewController
@property (nonatomic) int score;
@property (strong,nonatomic) NSString *name;
@end
