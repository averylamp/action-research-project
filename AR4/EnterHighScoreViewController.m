//
//  EnterHighScoreViewController.m
//  AR4
//
//  Created by Avery Lamp on 4/20/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "EnterHighScoreViewController.h"
#import "Highscore.h"
@interface EnterHighScoreViewController ()
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UITextField *TextboxForName;
@end

@implementation EnterHighScoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)EnterClicked:(UIButton *)sender {
    [self resignFirstResponder];
    self.name  = self.TextboxForName.text;
    /*
    NSString *filePath = @"Highscores";//file path...
    
    
    NSString *fileRoot = [[NSBundle mainBundle]
                          pathForResource:filePath ofType:@"txt"];
    
    NSString *fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    
    
    NSString *hsToWrite = [fileContents stringByAppendingString:[NSString stringWithFormat:@"\n%@-%d",self.name,self.score]];
     */
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/textfile.txt",
                          documentsDirectory];
    //create content - four lines of text
    //NSString *fileContent = @"One\nTwo\nThree\nFour\nFive";
    //save content to the documents directory
    
    
    
    //[content writeToFile:fileName
    //          atomically:NO
    //           encoding:NSStringEncodingConversionAllowLossy
    //            error:nil];
    NSString *fileContents = [[NSString alloc]initWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"File contents - %@",fileContents);
    if (!fileContents){
        fileContents = @"";
        NSLog(@"HERE");
    }
    NSString *hsToWrite = [fileContents stringByAppendingString:[NSString stringWithFormat:@"\n%@-%d",self.name,self.score]];
    NSLog(@"HS toWrite - %@",hsToWrite);
    if([fileContents isEqualToString:@""]){
        hsToWrite =[NSString stringWithFormat:@"%@-%d",self.TextboxForName.text,[self.scoreLabel.text intValue]];
    }
    
    [hsToWrite writeToFile:fileName atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"here");
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scoreLabel setText:[NSString stringWithFormat:@"Score : %d",self.score]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
