//
//  HighScoreViewController.m
//  AR4
//
//  Created by Avery Lamp on 4/12/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "HighScoreViewController.h"
#import "Highscore.h"
@interface HighScoreViewController ()
@property (strong, nonatomic) IBOutlet UITextView *HighScoreList;
@property (strong, nonatomic) NSMutableArray *highScores;

@end

@implementation HighScoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    /*
     NSString* filePath = @"Highscores";//file path...
     
     
     NSString* fileRoot = [[NSBundle mainBundle]
     pathForResource:filePath ofType:@"txt"];
     
     NSString* fileContents =
     [NSString stringWithContentsOfFile:fileRoot
     encoding:NSUTF8StringEncoding error:nil];
     */
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/textfile.txt",
                          documentsDirectory];
    //create content - four lines of text
    //NSString *fileContent = @"One\nTwo\nThree\nFour\nFive";
    //save content to the documents directory
    
    
    
    //[content writeToFile:fileName
    //          atomically:NO
    //           encoding:NSStringEncodingConversionAllowLossy
    //            error:nil];
    
    
    NSString *fileContents = [[NSString alloc]initWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    if (![fileContents isEqualToString:@""]){
        
        // first, separate by new line
        NSArray* allLinedStrings =
        [fileContents componentsSeparatedByCharactersInSet:
         [NSCharacterSet newlineCharacterSet]];
        
        
        NSMutableArray *tempHSList = [[NSMutableArray alloc]init];
        Highscore *nexths;
        Highscore *temp;
        for(NSString *s in allLinedStrings)
        {
            
            //NSLog(@"Here - %@",s);
            nexths = [[Highscore alloc] initWithString:s];
            for (int i=0; i < [tempHSList count]; i++)
            {
                temp = tempHSList [i];
                if(nexths.score > temp.score)
                {
                    [tempHSList insertObject:nexths atIndex:i];
                    break;
                }
            }
            if (![tempHSList containsObject:nexths])
            {
                [tempHSList addObject:nexths];
            }
            
        }
        
        self.highScores= [[NSMutableArray alloc]initWithArray:[tempHSList mutableCopy]];
        NSString *s = [[NSString alloc]init];
        for(Highscore *hs in self.highScores)
        {
            s = [[s stringByAppendingString:[NSString stringWithFormat:@"%d - %@", hs.score, hs.name]]stringByAppendingString:@"\n"];
        }
        self.HighScoreList.text = s;
        NSLog(@"%@",s);
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
