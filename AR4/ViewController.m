//
//  ViewController.m
//  AR4
//
//  Created by Avery Lamp on 4/12/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import "ViewController.h"
#import "MCQuestionGame.h"
#import "EnterHighScoreViewController.h"
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *QuestionLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AnswerButtons;
@property(strong, nonatomic) MCQuestionGame *game;
@property (strong, nonatomic) IBOutlet UIButton *NextButton;
@property(strong, nonatomic)Question *currentQuestion;
@property(nonatomic) int score;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UIButton *sourceButton;
@property (strong, nonatomic) IBOutlet UIView *timeBar;
@property ( nonatomic) double  timeSinceQuestioned;
@property (strong, nonatomic) IBOutlet UILabel *QuestionNumberLabel;
@property (nonatomic) int questionNumber;
@property (nonatomic) int questionsPerGame;
@end

@implementation ViewController
- (IBAction)sourceButtonClicked:(UIButton *)sender {
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.currentQuestion.source]];
    //NSLog(self.currentQuestion.source);
}

-(void) setQuestionNumber:(int)questionNumber
{
    _questionNumber= questionNumber;
    [self.QuestionNumberLabel setText:[NSString stringWithFormat:@"Question %d/%d",questionNumber, self.questionsPerGame]];
}

-(void) setScore:(int)score
{
    _score= score;
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", _score];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.questionsPerGame = 10;//Change to change the number of questions
    [self.game resetGame];
    for(UIButton *b in self.AnswerButtons){
        [b setHidden:YES];
        [b setEnabled:NO];
    }
    [self.QuestionLabel setHidden:YES];
    [self.NextButton setHidden:YES];
    [self.NextButton setEnabled:NO];
    [self.sourceButton setHidden: YES];
    
    
	[self loadNextQuestion];
    
    if (self.questionNumber > self.questionsPerGame){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)AnswerClicked:(UIButton *)sender {
   
    //NSLog(@"ANSWER CLICKED");
    [self.sourceButton setHidden: NO];
    if ([sender.titleLabel.text isEqualToString:self.currentQuestion.answer])
    {
        [sender setBackgroundColor:[UIColor greenColor]];
        [self.NextButton setHidden:NO];
        [self.NextButton setEnabled:YES];
        int timeScore = (int)(10 - (CACurrentMediaTime() - self.timeSinceQuestioned)) +1;
        
        self.score = self.score + 10 + timeScore;
        
    }else
    {
        [sender setBackgroundColor:[UIColor redColor]];
        for (UIButton *b in self.AnswerButtons)
        {
            if ([b.titleLabel.text isEqualToString:self.currentQuestion.answer])
            {
                
                [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    [b setBackgroundColor:[UIColor greenColor]];
                } completion:^(BOOL finished) {
                    if(finished){
                        [self.NextButton setHidden:NO];
                        [self.NextButton setEnabled:YES];
                    }
                }];
            }
        }
    }
    
    for( UIButton *b in self.AnswerButtons)
    {
        [b setEnabled:NO];
    }
    
    
    double ratio =1-(( CACurrentMediaTime() - self.timeSinceQuestioned) / 10);
    [self.timeBar.layer removeAllAnimations];
    
    [self.timeBar setFrame:CGRectMake(35, 65, 84, 743*ratio)];//400 * timesincequestioned/15
    
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"EnterHighScore"])
    {
        if (self.questionNumber > self.questionsPerGame){
            return YES;
        }else {
            return NO;
        }
    }else{
        return NO;
    }
    
    
}

- (IBAction)NextButtonClicked:(UIButton *)sender {
    if (self.questionNumber <= self.questionsPerGame){
        [self loadNextQuestion];
    }else
    {
        //get an imput for the high score
    }
    [self.NextButton setHidden:YES];
    [self.sourceButton setHidden: YES];
}

-(void) loadNextQuestion
{
    self.questionNumber ++;
    [UIView animateWithDuration:1 animations:^{
        self.timeBar.frame = CGRectMake(35, 65, 84, 743);  }];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{self.QuestionLabel.alpha = 0.0;}
                     completion:^(BOOL fin){
                         if(fin){
                             [self setNewTexts];
                             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{self.QuestionLabel.alpha = 1.0;}
                                                                                         completion:nil];
                         
                         }
                     }];
    
    for (UIButton *b in self.AnswerButtons)
    {
        [b setEnabled:NO];
        
        
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{b.alpha = 0.0;}
                         completion:^(BOOL fin){if(fin)
        {
            [UIView animateWithDuration:1 delay:3 options:UIViewAnimationOptionCurveEaseIn animations:^{b.alpha = 1.0;}    completion:^(BOOL fin){
                if (fin){
                    [self startTimer];
                    [b setEnabled:YES];
                }
            }];
            [b setHidden:NO];
        };
                         }];
        //NSLog(@"ANIMATING");
        
        [b setBackgroundColor:[UIColor colorWithRed:(float)172.0/255.0f green:(float)(210.0/255.0f) blue:(float)255.0/255.0f alpha:1.0]];
        
        
    }
    
}

-(void) startTimer
{
    
    CGRect tempRec = CGRectMake(35, 65, 84, 0);
    self.timeSinceQuestioned = CACurrentMediaTime();
    
    
    [UIView animateWithDuration:10.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.timeBar.frame = tempRec;
    } completion:^(BOOL finished) {
        
        if(![self.timeBar.layer.animationKeys count] && CGRectEqualToRect(self.timeBar.frame, CGRectMake(35, 65, 84, 0))){
            //NSLog(@"Time out");
            for (UIButton *b in self.AnswerButtons)
            {
                if ([b.titleLabel.text isEqualToString:self.currentQuestion.answer])
                {
                    
                    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        [b setBackgroundColor:[UIColor greenColor]];
                    } completion:^(BOOL finished) {
                        if(finished){
                            [self.NextButton setHidden:NO];
                            [self.sourceButton setHidden: NO];
                        }
                    }];
                }
            }
            for( UIButton *b in self.AnswerButtons)
            {
                [b setEnabled:NO];
            }
            
        }
        
    }];
    
    
    
}


-(void) setNewTexts
{
    
    Question *nextQuestion = [self.game getRandomQuestion];
    self.currentQuestion = nextQuestion;
    NSLog(@"YES");
    if(nextQuestion)
    {
        [self.QuestionLabel setText:nextQuestion.question];
        [self.QuestionLabel setHidden:NO];
        NSMutableArray *answers = [nextQuestion getRandomAnswers];
        unsigned index = arc4random() %3;
        if([answers containsObject:@"United States"]){
            [answers removeObject:@"United States"];
            [answers insertObject:@"United States" atIndex:index];
        }
        index = arc4random()%4;
        
        [answers insertObject:nextQuestion.answer atIndex:index];
        
        for (int i=0; i< [self.AnswerButtons count]; i++)
        {
            [self.AnswerButtons [i] setTitle:answers [i] forState:UIControlStateNormal];
            UIButton  *temp = self.AnswerButtons [i];
            [temp.titleLabel setHidden:NO];
            [temp setEnabled:YES];
            [temp setEnabled:NO];
        }

        
    }
    else{
        //NSLog(@"NO MORE QUESTIONS");
    }
    

}

- (MCQuestionGame *) game
{
    if (!_game)
    {
        _game = [[MCQuestionGame alloc] initWithNumberToAsk:10];
    }
    return _game;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"EnterHighScore"])
    {
        if([segue.destinationViewController isKindOfClass:[EnterHighScoreViewController class]])
        {
            EnterHighScoreViewController *ehsvc = (EnterHighScoreViewController *) segue.destinationViewController;
            ehsvc.score = self.score;
            
        }
    }
    
}


@end
