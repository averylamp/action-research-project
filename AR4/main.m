//
//  main.m
//  AR4
//
//  Created by Avery Lamp on 4/12/14.
//  Copyright (c) 2014 Avery Lamp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
